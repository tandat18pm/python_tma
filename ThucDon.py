import json
import random
danh_sach_nhom=[]
sl_mon=[]
with open('mon_an.json', encoding='utf-8') as data_file:
    data = json.loads(data_file.read())

def Random_nhom():# Chọn ngẫu nhiên nhóm thức ăn
    for i in data:
        danh_sach_nhom.append(i)
    x=random.choice(danh_sach_nhom)    
    return x

def Chon_Mon():#Chọn ngẫu nhiên món ăn thuộc nhóm thức ăn phía trên
    loai_mon=Random_nhom()
    for i in range (0,1):
        x=random.choice(data[loai_mon]) 
    return x

def Chon_Canh():#Chọn ngẫu nhiên món canh từ nhóm thức ăn soup
    for i in range (0,1):
        x=random.choice(data['soup'])
        # bua_sang.append(x)
        # print(bua_sang)    
    return x

def Bua_sang():#Bữa sáng được chọn ngẫu nhiên 3 món không trùng nhau từ những nhóm thức được chọn ngẫu nhiên 
    bua_sang=[]
    for i in range(1,10):
        x=Chon_Mon()
            # print(Random_nhom())
        if(len(bua_sang)<3):
            if x not in bua_sang and x !='--- Baemin ---' and x!= '-- Nhịn --':
                bua_sang.append(x)
            else:
                continue
        else:
            break

    return bua_sang

def Bua_trua():#Bữa trưa gồm món chính được chọn ngẫu nhiên nhưng phải khác nhóm soup, và đi kèm 2 món canh thuộc nhóm soup
    bua_trua=[]
    for i in range(1,100):
        mon_chinh=Chon_Mon()
        if mon_chinh not in data['soup']:            
            if len(bua_trua)<1:
                bua_trua.append(mon_chinh)
        else:
            continue
    for j in range(1,100):
        canh=Chon_Canh()
        if canh not in  bua_trua:
            if len(bua_trua)<3:
                bua_trua.append(canh)
        else:
            continue
    
    return bua_trua
def convert_list_to_string(list, seperator=', '):# chuyển từ list sang string
    return seperator.join(list)
def OutPut():
    print("~~~~~~~~~~~~ Bữa Sáng ~~~~~~~~~~")
    for i in range(2,9):
        
        if i==8:
            print("Chủ nhật",": ",convert_list_to_string(Bua_sang()))
        else:
            print("Thứ",i,"   : ",convert_list_to_string(Bua_sang()))
    print("~~~~~~~~~~~~ Bữa Trưa ~~~~~~~~~~")
    for i in range(2,9):
        
        if i==8:
            print("Chủ nhật",": ",convert_list_to_string(Bua_trua()))
        else:
            print("Thứ",i,"   : ",convert_list_to_string(Bua_trua()))

OutPut()